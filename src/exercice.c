#include <stdio.h>
#include <string.h>

typedef struct arg_opt {
	char * name;
	void * fn;
} Arg_opt;

Arg_opt options[1] = {{"toto", NULL}};

int main(int argc, char ** argv) {
	int arg_idx = argc;
	int opt_idx;
	char * txt;
	char * arg;


	while(arg_idx > 0) {
		printf("arg_idx=%d\n", arg_idx);
		arg = argv[arg_idx-1];
		opt_idx = sizeof(options) / sizeof(options[0]);
		while (opt_idx > 0) {
			printf("opt_idx=%d\n", opt_idx);
			txt = options[opt_idx-1].name;
			if (strcmp(txt, arg)==0) {
					printf("[+] %s\n", arg);
			}
			opt_idx--;
		}
		arg_idx--;
	}
	return 0;
}
