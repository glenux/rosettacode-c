#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct hashcell_t {
	char key[128];
	void * value;
	struct hashcell_t* next;
} HASHCELL;

typedef struct {
	int capacity;
	int size;
	HASHCELL** data;
} HASHTABLE;

int hash_of_key(HASHTABLE* hashtable, char * key) {
	int val=0;
	int idx=0;
	while (key[idx] != '\0') {
		val ^= key[idx];
		idx++;
	}
	return (val % hashtable->capacity);
}

HASHTABLE* hashtable_create(int capacity) {
	HASHTABLE * hashtable;
	printf("hashtable_create -- %d\n", capacity);

	hashtable = malloc(sizeof(HASHTABLE));
	if (!hashtable) return NULL;
	memset(hashtable, 0, sizeof(HASHTABLE));

	hashtable->data = malloc(capacity * sizeof(HASHCELL*));
	if (!hashtable->data) return NULL;
	memset(hashtable->data, 0, capacity * sizeof(HASHCELL*));

	hashtable->capacity = capacity;
	hashtable->size = 0;

	printf("hashtable_create -- done\n");
	return hashtable;
}

void hashtable_destroy(HASHTABLE* hashtable) {
	printf("hashtable_destroy(%p) -- \n", (void*)hashtable);
	if (!hashtable) return;
	if (!hashtable->data) return;
	free(hashtable->data);
	free(hashtable);
	printf("hashtable_destroy(%p) -- done\n", (void*)hashtable);
}

void hashtable_rehash(HASHTABLE* hashtable) {
	return;
}

int hashtable_set(HASHTABLE* hashtable, char* key, void * value) {
	int idx;
	HASHCELL* cell;

	printf("hashtable_set(%s, %p) -- \n", key, (void*)value);
	// get position in hashtable
	idx = hash_of_key(hashtable, key);
	hashtable->size += 1;

	printf("hashtable_set -- index [%d/%d]\n", idx, hashtable->capacity);
	// search value
	cell = hashtable->data[idx];
	while(cell) {
		if (strcmp(cell->key, key) == 0) { break; }
		cell = cell->next;
	}

	if (cell) {
		// if cell exists, then change inner value
		printf("hashtable_set -- update value\n");
		cell->value = value;
	} else {
		printf("hashtable_set -- create value\n");
		// create cell & prepend it to the chain
		cell = malloc(sizeof(HASHCELL));
		if (!cell) return -1;
		memset(cell, 0, sizeof(HASHCELL));

		cell->next = hashtable->data[idx];
		cell->value = value; 
		strcpy(cell->key, key);
		hashtable->data[idx] = cell;
		printf("hashtable_set -- cell@%p{key=%s, value=%p, next=%p}\n", 
				(void*)cell,
				cell->key,
				(void*)cell->value,
				(void*)cell->next);
	}
	printf("hashtable_set(...) -- done\n");

	return 0;
}

void * hashtable_get(HASHTABLE* hashtable, char* key) {
	int idx;
	HASHCELL* cell;
	printf("hashtable_get(%p, %p) -- \n", hashtable, key);
	idx = hash_of_key(hashtable, key);
	printf("hashtable_get(...) -- index [%d/%d]\n", idx, hashtable->capacity);
	cell = hashtable->data[idx];
	while(cell) {
		printf("hashtable_get(...) -- cell@%p{key=%s, value=%p, next=%p}\n", 
				(void*)cell,
				cell->key,
				cell->value,
				cell->next);
		if (strcmp(cell->key, key) == 0) { break; }
		cell = cell->next;
	}
	if (cell) { 
		return cell->value;
	} else {
		printf("hashtable_get(...) -- cell not found\n");
		return NULL;
	}
}

int main(int argc, char** argv) {
	HASHTABLE * hashtable;
	int x=1, y=2, z=3;
	int* value;

	printf("Run !\n");
	hashtable = hashtable_create(1000);
	hashtable_destroy(hashtable);

	hashtable = hashtable_create(100);
	hashtable_set(hashtable, "alice", &x);
	printf("h['alice'] <- %d\n", x);
	hashtable_set(hashtable, "bob", &y);
	printf("h['bob'] <- %d\n", y);
	hashtable_set(hashtable, "charlie", &z);
	printf("h['charlie'] <- %d\n", z);
	hashtable_set(hashtable, "alice", &y);
	printf("h['alice'] <- %d\n", y);

	value = hashtable_get(hashtable, "alice"); 
	printf("h['alice'] ? %d\n", *value);
	value = hashtable_get(hashtable, "bob"); 
	printf("h['bob'] ? %d\n", *value);
	value = hashtable_get(hashtable, "charlie"); 
	printf("h['charlie'] ? %d\n", *value);

	return EXIT_SUCCESS;
}

