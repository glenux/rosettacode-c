
#include <minunit/minunit.h>
#include <libcsv/libcsv.h>

  /*
	CSV * csv;

	printf("%s\n%s\n\n",TITLE, URL);

	csv = csv_create(0, 0);
	csv_open(csv, "fixtures/csv-data-manipulation.csv");
	csv_display(csv);

	csv_set(csv, 0, 0, "Column0");
	csv_set(csv, 1, 1, "100");
	csv_set(csv, 2, 2, "200");
	csv_set(csv, 3, 3, "300");
	csv_set(csv, 4, 4, "400");
	csv_display(csv);

	csv_save(csv, "tmp/csv-data-manipulation.result.csv");
	csv_destroy(csv);
*/

MU_TEST(test_check) {
    mu_check(5 == 7);
}

MU_TEST_SUITE(test_suite) {
    MU_RUN_TEST(test_check);
}

/** 
 * Test
 */
int main(int argc, char ** argv) {
    MU_RUN_SUITE(test_suite);
    MU_REPORT();
	return 0;
}

