
SRC       = $(wildcard src/*.c)
DEPS      = $(wildcard deps/**/*.c)
LIBS      = $(wildcard lib/**/*.c)
OBJS      = $(patsubst %.c,%.o,$(DEPS) $(LIBS))
BINS      = $(patsubst src/%,bin/%,$(patsubst %.c,%,$(SRC)))

TEST_SRC  = $(wildcard test/*.c)
TEST_BINS = $(patsubst test/%,bin/%,$(patsubst %.c,%.test,$(TEST_SRC)))

# CFLAGS  = -std=c99 -Ideps -Wall -Wno-unused-function -U__STRICT_ANSI__
CFLAGS  = -ggdb -std=c99 -Ideps -Ilib -Wall -Wno-unused-function -pedantic -D_POSIX_C_SOURCE=200112L


all: $(BINS) $(TEST_BINS)
	echo "BINS=$(BINS)"
	echo "TESTS=$(TEST_BINS)"

$(BINS): $(SRC) $(OBJS)
	$(CC) $(CFLAGS) -o $@ src/$(notdir $@).c $(OBJS) $(LDFLAGS)

$(TEST_BINS): $(TEST_SRC) $(OBJS)
	$(CC) $(CFLAGS) -o $@ test/$(notdir $(patsubst %.test,%.c,$@)) $(OBJS) $(LDFLAGS)

%.o: %.c
	$(CC) $< -c -o $@ $(CFLAGS)

clean:
	$(foreach c, $(BINS), rm -f $(c);)
	$(foreach c, $(TEST_BINS), rm -f $(c);)
	rm -f $(OBJS)
