
#define _GNU_SOURCE
#define bool int
#include <stdio.h>
#include <stdlib.h> /* malloc...*/
#include <string.h> /* strtok...*/
#include <ctype.h>
#include <errno.h>


typedef struct {
	char * delim;
	unsigned int rows;
	unsigned int cols;
	char ** table;
} CSV;


/* libcsv.c */
int trim(char **str);
int csv_destroy(CSV *csv);
CSV *csv_create(unsigned int cols, unsigned int rows);
char *csv_get(CSV *csv, unsigned int col, unsigned int row);
int csv_set(CSV *csv, unsigned int col, unsigned int row, char *value);
void csv_display(CSV *csv);
int csv_resize(CSV *old_csv, unsigned int new_cols, unsigned int new_rows);
int csv_open(CSV *csv, char *filename);
int csv_save(CSV *csv, char *filename);
